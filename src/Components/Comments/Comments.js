import React, { Component } from "react";
import Avatar from "../Avatar/Avatar";
import PropTypes from "prop-types";
import Comment from "../Comment/comment";

function Comments(props) {
  var comments = props.comments.map((comment, index) => (
    <Comment
      key={`comment-${index}`}
      name={comment.name}
      comment={comment.comment}
      image={comment.image}
    />
  ));
  return <div>{comments}</div>;
}

Comments.propTypes = {
  comments: PropTypes.array.isRequired
};

export default Comments;
