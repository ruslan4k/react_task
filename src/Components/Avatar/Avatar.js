import PropTypes from "prop-types";
import React, { Component } from "react";
import "./Avatar.scss";

function Avatar(props) {
  return (
    <img
      src={"src/assets/" + props.image}
      alt="User's picture"
      className="align-self-center avatar-image"
    />
  );
}

Avatar.propTypes = {
  image: PropTypes.string.isRequired
};

export default Avatar;
