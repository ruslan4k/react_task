import React, { Component } from "react";
import Avatar from "../Avatar/Avatar";
import PropTypes from "prop-types";
import "./CommentForm.scss";

var comment = { image: "avatar1.jpg", comment: "", name: "Edward Crane" };

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.props.isFetching) return;
    this.props.addComment({ ...comment, comment: this.state.value });
  }

  componentWillReceiveProps(nextProps) {
    // if comment had been succesfull added - reset value
    if (!nextProps.error && !nextProps.isFetching) this.setState({ value: "" });
  }

  render() {
    return (
      <div>
        <div className="media mt-4">
          <Avatar image="avatar1.jpg" />
          <form className="media-body" onSubmit={this.handleSubmit}>
            <div className="form-group">
              <input
                className="form-control mt-3"
                type="text"
                value={this.state.value}
                onChange={this.handleChange}
                placeholder="Enter comment"
              />
            </div>
          </form>
        </div>
        <div className="comment-form-error-text text-danger">{this.props.error}</div>
        <hr className="comment-form-line" />
      </div>
    );
  }
}

CommentForm.propTypes = {
  error: PropTypes.string,
  isFetching: PropTypes.bool
};

export default CommentForm;
