import React, { Component } from "react";
import Avatar from "../Avatar/Avatar";
import PropTypes from "prop-types";
import "./comment.scss";

function Comment(props) {
  return (
    <div className="media comment mt-4">
      <Avatar image={props.image} />
      <div className="media-body">
        <h5>{props.name}</h5>
        {props.comment}
        <div>
          <a className="comment-option">
            Like (15)
          </a>
          <a  className="comment-option">
            Reply
          </a>
          <a className="comment-option">
            View Replies (4)
          </a>
        </div>
      </div>
    </div>
  );
}

Comment.propTypes = {
  image: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default Comment;
