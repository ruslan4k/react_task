import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Spinner.scss";

function Spinner(props) {
  return (
   props.isShown && (
      <i className="fa fa-spinner fa-spin spinner-icon"/>
    )
  );
}

Spinner.propTypes = {
  isShown: PropTypes.bool.isRequired
};

export default Spinner;
