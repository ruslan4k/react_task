import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import commentsReducer  from './reducers/commentsReducer';
import { hashHistory } from 'react-router'
import thunk from "redux-thunk";

let initialState = {};

const Store = createStore(
    combineReducers({
        routing: routerReducer,
        comments: commentsReducer
    }),
    initialState,
    compose(
        applyMiddleware(routerMiddleware(hashHistory), thunk)
    )
);


export default Store;

