import * as ACTIONS from "./actionTypes";

export function add_comment(comment) {
  return function(dispatch) {
    dispatch(add_comment_request());
    fakeAPIcall(comment)
      .then(comment => {
        dispatch(add_comment_success(comment));
      })
      .catch(error => {
        console.log(error);
        let errorMessage = error.message;
        dispatch(add_comment_failure(errorMessage));
      });
  };
}

var fakeAPIcall = comment => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (comment.comment.length > 25) {
        return reject(new Error("Comment can't be more than 25 symbols"));
      } else if (comment.comment.length === 0) {
        return reject(new Error("Comment can't be blank"));
      } else {
        return resolve(comment);
      }
    }, 1500);
  });
};

function add_comment_request() {
  return {
    type: ACTIONS.ADDING_COMMENT_REQUEST
  };
}

function add_comment_success(comment) {
  return {
    type: ACTIONS.ADDING_COMMENT_SUCCESS,
    comment
  };
}

function add_comment_failure(error) {
  return {
    type: ACTIONS.ADDING_COMMENT_FAILURE,
    error
  };
}
