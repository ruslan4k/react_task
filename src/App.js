import React, { Component } from "react";
import { connect } from "react-redux";
import CommentForm from "./Components/CommentForm/CommentForm";
import Comments from "./Components/Comments/Comments";
import Spinner from './Components/Spinner/Spinner';
import { add_comment } from "./actions/commentsActions";
import "./App.scss";

/**
 * Main Wrapper
 */
class App extends Component {
  render() {
    const { add_comment, error, comments, isFetching } = this.props;
    return (
      <div className='container'>
        <h3>Comments</h3>
        <CommentForm
          addComment={add_comment}
          error={error}
          isFetching={isFetching}
        />
        <Spinner isShown={isFetching}/>
        <Comments comments={comments} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    comments: state.comments.comments,
    isFetching: state.comments.isFetching,
    error: state.comments.error
  };
};

export default connect(
  mapStateToProps,
  { add_comment }
)(App);
