import * as ACTIONS from "../actions/actionTypes";

var initialState = {
  isFetching: false,
  comments: [
    {
      image: "avatar2.jpg",
      comment:
        "I really like the way you have broken down the material. How does one model the transition state?",
      name: "Janet Ryan"
    },
    {
      image: "avatar3.jpg",
      comment:
        "Have you ever tried to carry out an experiment to determine if the flow is turbulent or not?",
      name: "Bob Ceslo"
    }
  ]
};

export default function commentsReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.ADDING_COMMENT_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: null
      };
    case ACTIONS.ADDING_COMMENT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error
      };
    case ACTIONS.ADDING_COMMENT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        comments: [...state.comments, action.comment]
      };
    default:
      return state;
  }
}
